package com.woniu.task.remind.task;

import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;

/**
 * 推荐一个日志管理神器！ 这款 IDEA 插件太好用了！Grep Console
 * @author 蜗牛
 * @description 每天进步一点点
 * @date 2023/03/18 13:48
 */
@Slf4j
@Component
public class DrinkTask  {

    @Scheduled(cron = "0/5 * * * * ?")
    public void execute() {
        log.info("[喝水提醒]主人,记得喝水哦"+ LocalDateTime.now());

    }
}
