package com.woniu.service.util;

import org.lionsoul.ip2region.xdb.Searcher;

public class AddressUtil {


    /**
     * 根据IP地址查询登录来源
     *
     * @param ip
     * @return
     */
    public static String getCityInfo(String ip) {
        try {
            Searcher searcher = Searcher.newWithFileOnly("ip2region/ip2region.xdb");
            //开始查询
            return searcher.searchByStr(ip);
        } catch (Exception e) {
            e.printStackTrace();
        }
        //默认返回空字符串
        return "";
    }

    public static void main(String[] args) {
        //204.16.111.255
        System.out.println(getCityInfo("204.16.111.255"));
    }

}