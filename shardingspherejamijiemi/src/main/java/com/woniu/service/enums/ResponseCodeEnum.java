package com.woniu.service.enums;

/**
 * <p>
 *  状态枚举类
 * </p>
 *
 * @author 公众号：【程序员蜗牛g】
 */
public enum ResponseCodeEnum {

    SUCCESS(0, "0", "成功"),
    FAIL (-1, "-1", "失败"),
    PARAMETER_ERROR(1,"1","参数错误"),
    ERROR(500, "500", "错误");

    private final int id;
    private final String code;
    private final String label;

    ResponseCodeEnum(final int id, final String code, final String label) {
        this.id = id;
        this.code = code;
        this.label = label;
    }

    public int getId() {
        return id;
    }

    public String getCode() {
        return code;
    }

    public String getLabel() {
        return label;
    }

}
