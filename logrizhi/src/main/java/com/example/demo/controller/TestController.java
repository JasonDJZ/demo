package com.example.demo.controller;

import com.example.demo.service.AopLogService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 超简洁！SpringBoot使用AOP统一日志管理
 */
@RestController
@RequestMapping("/api")
@Slf4j
public class TestController {

    private final AopLogService aopLogService;

    public TestController(AopLogService aopLogService) {
        this.aopLogService = aopLogService;
    }

    @GetMapping("/test/{id}")
    public ResponseEntity<String> test(@PathVariable("id") Integer id) {
        return ResponseEntity.ok().body(aopLogService.test(id));
    }

}
